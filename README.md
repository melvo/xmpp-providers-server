<!--
SPDX-FileCopyrightText: 2024 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# XMPP Providers Server - Server Setup of [XMPP Providers](https://providers.xmpp.net)

[![Green Web Check](https://api.thegreenwebfoundation.org/greencheckimage/xmpp-providers.org?nocache=true)](https://www.thegreenwebfoundation.org/green-web-check/?url=xmpp-providers.org)

[![REUSE status](https://api.reuse.software/badge/invent.kde.org/melvo/xmpp-providers-server)](https://api.reuse.software/info/invent.kde.org/melvo/xmpp-providers-server)

[TOC]

## Introduction

This project contains an automated server setup used by [XMPP Providers](https://invent.kde.org/melvo/xmpp-providers).
It makes use of [Ansible](https://www.ansible.com) to set up a [Debian](https://www.debian.org)-based server.

The contained [playbooks](#playbooks) are an all-in-one setup for fully-featured [XMPP](https://xmpp.org/about/technology-overview/) servers.
They focus on simplicity rather than on covering all possible use cases.
That is done by using sensible defaults with strong security properties.
But the playbooks can also be used as a good base for more complex setups if needed.

## Playbooks

This section describes the usable [playbooks](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#playbooks) and their [roles](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#roles).
The roles contain [tasks](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#tasks) for applying desired changes and [handlers](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#handlers) to react to those changes (e.g., by restarting a system service).

Several tasks use server-specific variables.
Each role defines default variables.
They can be overwritten for each server.

### Init Playbook

The [**Init** playbook](/ansible/init_playbook.yml) initializes your [inventory](https://docs.ansible.com/ansible/latest/getting_started/basic_concepts.html#inventory) (i.e., your local files for configuring the servers).

It contains the following roles:

1. **[inventory](/ansible/roles/inventory/)**: Creates the inventory file `ansible/hosts.yml` for listing the servers
1. **[defaults](/ansible/roles/defaults/)**: Creates inventory variable files for the servers (i.e., `ansible/host_vars/<server>` (e.g., `ansible/host_vars/xmpp-providers.org.yml`) with default values for all other roles coming from their default files (`ansible/roles/*/defaults/*.yml`) such as [ejabberd's default file](/ansible/roles/ejabberd/defaults/main.yml))

### Local Playbook

The [**Local** playbook](/ansible/local_playbook.yml) configures your local setup for connecting to the servers.
It uses the variables that you adjusted to your needs after running the [Init playbook](#init-playbook).

It contains the following roles:

1. **[ssh](/ansible/roles/ssh/)**: Configures the local [SSH](https://en.wikipedia.org/wiki/Secure_Shell) setup via the OpenSSH client [ssh](https://man.openbsd.org/ssh.1)

### Remote Playbook

The [**Remote** playbook](/ansible/remote_playbook.yml) configures the servers.

It contains the following roles:

1. **[system](/ansible/roles/system/)**: Manages the server's operating system
1. **[network](/ansible/roles/network/)**: Manages the network settings
1. **[apt](/ansible/roles/apt/)**: Configures the software repository via Debian's package manager [apt](https://wiki.debian.org/Apt)
1. **[sshd](/ansible/roles/sshd/)**: Configures the server's SSH setup via the OpenSSH server [sshd](https://man.openbsd.org/sshd.8)
1. **[certbot](/ansible/roles/certbot/)**: Configures the [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) usage with certificates issued by the certificate authority ([CA](https://en.wikipedia.org/wiki/Certificate_authority)) [Let's Encrypt](https://letsencrypt.org) via the [ACME](https://en.wikipedia.org/wiki/Automatic_Certificate_Management_Environment) client [certbot](https://certbot.eff.org)
1. **[sslh](/ansible/roles/sslh/)**: Forwards client requests from the HTTPS port `443` to ports used by server applications via the protocol demultiplexer [sslh](https://www.rutschle.net/tech/sslh/README.html) (enabling clients to connect to the server even if they are behind restricted firewalls allowing HTTPS but not the several protocols the server software uses)
1. **[postgresql](/ansible/roles/postgresql/)**: Configures an [SQL](https://en.wikipedia.org/wiki/SQL) database via the database management system ([DBMS](https://en.wikipedia.org/wiki/Database#Database_management_system)) [PostgreSQL](https://www.postgresql.org) to store the XMPP server's data
1. **[ejabberd](/ansible/roles/ejabberd/)**: Configures the XMPP server via [ejabberd](https://www.ejabberd.im)
1. **[ejabberd_accounts](/ansible/roles/ejabberd_accounts/)**: Manages XMPP accounts
1. **[dns](/ansible/roles/dns/)**: Helps to create [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) records

## Run Scripts

Each playbook is run by a shell script (called **run script**).

The name of the run script corresponds to its playbook:

* [`init.sh`](init.sh): Runs the [Init playbook](#init-playbook)
* [`local.sh`](local.sh): Runs the [Local playbook](#local-playbook)
* [`remote.sh`](remote.sh): Runs the [Remote playbook](#remote-playbook)

A run script internally uses [`ansible-playbook`](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) while providing it with necessary arguments.
All arguments you pass to a run script are passed to `ansible-playbook` as additional arguments.
That makes it possible to adjust the command to your needs.

Example for running the Remote playbook without configuring the server but displaying all changes:

```shell
./remote.sh -CD
```

Example for running the Remote playbook only on `example.org` and `example.com` even if there are more servers specified in your inventory file:

```shell
./remote.sh -l "example.org,example.com"
```

## Usage

This section explains the steps needed to configure your local setup and the XMPP Providers server.

The tasks are [idempotent](https://docs.ansible.com/ansible/latest/reference_appendices/glossary.html#term-Idempotency).
Thus, everything can be run each time the configuration files or the tasks themselves have been modified.
Changes are automatically detected and applied.

If Ansible hangs on any task, you can run the playbook again.
But keep in mind that some tasks need some time to complete.

Make sure to have the following dependencies installed:

* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html): Runs the playbook and its tasks (including community modules, usually a package called `ansible` contains the community modules instead of `ansible-core`)
* [Paramiko](https://www.paramiko.org/installing.html): Used by Ansible to connect via SSH to the server

The commands in this section expect you to be in the repositiory's directory:

```shell
cd xmpp-providers-server
```

### New Server

The following steps are done by the first person who connects to the server:

1. Run the [Init playbook](#init-playbook) (you can ignore the initial warning/s about the inventory source since there is no inventory file yet at that time):

    ```shell
    ./init.sh
    ```

    1. Enter `xmpp-providers.org` when you are asked for servers to be managed
1. Ensure that all settings for an old public server SSH key used to connect to `xmpp-providers.org` are reset in case you already used a server with the same domain or IP addresses (you can ignore the output):

    ```shell
    ssh-keygen -R "xmpp-providers.org"
    ssh-keygen -R "45.157.179.214"
    ssh-keygen -R "2a03:4000:4b:b7::1"
    ```

1. Retrieve the public server SSH key to locally authorize it for further connections and to hand it out to [other users](#first-login) (replace `<user>` with your saved server user):

    ```shell
    ssh <user>@xmpp-providers.org -o PubkeyAuthentication=no -o=HostKeyAlgorithms=ssh-ed25519 "cat /etc/ssh/ssh_host_ed25519_key.pub | awk '{print \$1,\$2}'"
    ```

    1. Enter the saved public server SSH key fingerprint (e.g., `SHA256:StPKQlWgh4eHuF9jISXSq03xS3bG+1dO46JHazYWSos` instead of simply entering `yes`) in order to properly verify the key's authenticity and to mitigate [MITM attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack):

        ```console
        alice@notebook:~/xmpp-providers-server$ ssh admin@xmpp-providers.org -o PubkeyAuthentication=no -o=HostKeyAlgorithms=ssh-ed25519 "cat /etc/ssh/ssh_host_ed25519_key.pub | awk '{print \$1,\$2}'"
        The authenticity of host 'xmpp-providers.org (45.157.179.214)' can't be established.
        ED25519 key fingerprint is SHA256:StPKQlWgh4eHuF9jISXSq03xS3bG+1dO46JHazYWSos.
        This key is not known by any other names.
        Are you sure you want to continue connecting (yes/no/[fingerprint])? SHA256:StPKQlWgh4eHuF9jISXSq03xS3bG+1dO46JHazYWSos
        ```

    1. Enter the server user password:

        ```console
        alice@notebook:~/xmpp-providers-server$ ssh admin@xmpp-providers.org -o PubkeyAuthentication=no -o=HostKeyAlgorithms=ssh-ed25519 "cat /etc/ssh/ssh_host_ed25519_key.pub | awk '{print \$1,\$2}'"
        ...
        Warning: Permanently added 'xmpp-providers.org' (ED25519) to the list of known hosts.
        admin@xmpp-providers.org's password:
        ```

    1. Save the remote command's output (i.e., the last printed line staring with `ssh-ed25519`) containing the public server SSH key that is needed in the next step to set the variable `ssh_server_key`:

        ```console
        alice@notebook:~/xmpp-providers-server$ ssh admin@xmpp-providers.org -o PubkeyAuthentication=no -o=HostKeyAlgorithms=ssh-ed25519 "cat /etc/ssh/ssh_host_ed25519_key.pub | awk '{print \$1,\$2}'"
        ...
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDUQYYDwLzHZYiR1ZR2cksc+gCjt51HUaduPTwrqj5zB
        ```

1. Adjust your inventory variables in `ansible/host_vars/xmpp-providers.org.yml` (have a look at the [example config file](/ansible/host_vars/xmpp-providers.org.example.yml), variables with `"{{ <value> }}"` take the values from other vairables)
1. Run the [Local playbook](#local-playbook):

    ```shell
    ./local.sh
    ```

    1. If a private SSH key is created, store its password in a password manager as the log output states
    1. If a public SSH key is created, add it to your inventory variable file `ansible/host_vars/xmpp-providers.org.yml` as the log output states
1. Copy your public SSH key to the server in order to be authorized to access the server without a password (replace `<SSH key file>` with the value of the variable `ssh_key_file` from `ansible/host_vars/xmpp-providers.org.yml`) - Additional persons can be authorized to access the server via [subsequent logins](#subsequent-logins) by a person whose SSH key is already authorized by the server if the variable `ssh_user_keys` contains the additional person's public SSH key:

    ```shell
    ssh-copy-id -i <SSH key file> xmpp-providers.org
    ```

    1. Enter the server user password:

        ```console
        alice@notebook:~/xmpp-providers-server$ ssh-copy-id -i ~/.ssh/id_ed25519 xmpp-providers.org
        /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/alice/.ssh/id_ed25519.pub"
        /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
        /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
        admin@xmpp-providers.org's password:
        ```

1. Log in to the server via the SSH key in order to check whether the connection is correctly configured and immediately log out afterwards (enter the SSH key password if you are asked to unlock it but if you are asked for the server user password, your configuration is incorrect) - Your SSH key is automatically added to the [OpenSSH agent](https://man.openbsd.org/ssh-agent.1) in order to be used by SSH connections including Ansible's SSH connection:

    ```shell
    ssh xmpp-providers.org "exit"
    ```

1. Run the [Remote playbook](#remote-playbook):

    ```shell
    ./remote.sh
    ```

The server is now configured to be only accessible via SSH keys (i.e., without passwords).
Since the server knows your SSH key now, you can log in with it for all [subsequent logins](subsequent-logins).

### Configured Server

This section explains how to connect to the server and manage it if it is already [intially configured](#new-server).

#### First Login

The following steps are done by everyone who does not yet have access to the server:

1. Get the files `ansible/hosts.yml` and `ansible/host_vars/xmpp-providers.org.yml` from the respository of a person who is already authorized to access the server and add those files to your repository
1. Adjust the variable `ssh_key_file` in `ansible/host_vars/xmpp-providers.org.yml` to point to your (existing or to be created) private SSH key
1. Ensure that all settings for an old public server SSH key used to connect to `xmpp-providers.org` are reset in case you already used a server with the same domain or IP addresses (you can ignore the output):

    ```shell
    ssh-keygen -R "xmpp-providers.org"
    ssh-keygen -R "45.157.179.214"
    ssh-keygen -R "2a03:4000:4b:b7::1"
    ```

1. Run the [Local playbook](#local-playbook):

    ```shell
    ./local.sh
    ```

    1. If a private SSH key is created, store its password in a password manager as the log output states
    1. If a public SSH key is created, add it to your inventory variable file `ansible/host_vars/xmpp-providers.org.yml` as the log output states
1. Give your public SSH key to all other persons who manage the server, they need to add it to their inventory variable file before running the [Remote playbook](#remote-playbook) the next time as well (otherwise, they will revoke your access because of Ansible's idempotency)
1. A person whose SSH key is already authorized by the server needs to execute `./remote.sh` for authorizing you to log in to the server
1. Get the server user password from a person who already manages the server and store it in a password manager to enter it in the next step and each time you log in again
1. Log in to the server via the SSH key in order to check whether the connection is correctly configured and immediately log out afterwards (enter the SSH key password if you are asked to unlock it but if you are asked for the server user password, your configuration is incorrect) - Your SSH key is automatically added to the [OpenSSH agent](https://man.openbsd.org/ssh-agent.1) in order to be used by SSH connections including Ansible's SSH connection:

    ```shell
    ssh xmpp-providers.org "exit"
    ```

1. Run the [Remote playbook](#remote-playbook):

    ```shell
    ./remote.sh
    ```

#### Subsequent Logins

Everyone who already has access to the server can simply run the [Remote playbook](#remote-playbook):

```shell
./remote.sh
```

## Manual Access

This sections is about manually accessing the server via SSH.
That is needed in case of any problem that cannot be fixed via the Ansible playbook.
Those manual changes cannot be simply applied again later by running the playbook.
Thus, **manually modifying the server should be avoided unless there is a good reason.**

Before you run the playbook for the first time, you can connect to the server via `ssh <user>@xmpp-providers.org` (replace `<user>` with your saved server user) and entering the server user's password.
After you run the playbook, you can connect to the server without entering the server user's password (your SSH key authenticates you).

### Commands

Here are some useful commands you can execute on the server.

Check a service's status (replace `<service>` with the service whose status you want to see):

```shell
sudo systemctl status <service>
```

Show a service's log (replace `<service>` with the service whose log you want to see):

```shell
sudo journalctl -xe -u <service>
```

Restart a service (replace `<service>` with the service you want to restart):

```shell
sudo systemctl restart <service>
```

Restart the server:

```shell
sudo reboot
```

To log out, press *Ctrl+D*.

## Status

There is a [status page](https://www.netcup-status.de) to see whether there are problems with the hosting provider.

The server is monitored by [CertWatch](https://certwatch.xmpp.net).
If the TLS certificate's public key changes, it will be detected.
The status can be subscribed to via [PubSub](xmpp:certwatch.xmpp.net?pubsub;action=subscribe;node=xmpp-providers.org).
Alternativaly, to receive status updates via normal messages, a bot can be [contacted](xmpp:certwatch.xmpp.net?message;body=subscribe%20xmpp-providers.org).

The server's features are tested via the [XMPP Compliance Tester](https://compliance.conversations.im/server/xmpp-providers.org/)

## Setup

This section is about setting the server up for the first time.
Afterwards, you can continue with [configuring and using the server](#usage).

The services are hosted on a [root server](https://www.netcup.de/bestellen/produkt.php?produkt=2892) virtualized via [KVM](https://linux-kvm.org/page/Main_Page):

* CPU: AMD EPYC™ 7702 (4 dedicated cores)
* RAM: 8 GB DDR4 ECC
* SSD: 160 GB RAID10

The server is reachable via the [domain](https://www.netcup.de/bestellen/produkt.php?produkt=20) `xmpp-providers.org` or via the following IP addresses:

* IPv4: `45.157.179.214`
* IPv6: `2a03:4000:4b:b7::1`

### Customer Control Panel

This section describes the steps on the Customer Control Panel ([CCP](https://www.customercontrolpanel.de)).
They are needed to add DNS records to the hosting provider's DNS server.

#### Access

Via Master Data -> Two-factor authentication (2FA):

1. Click *Enable two-factor authentication*
1. Scan the QR code with a [2FA](https://en.wikipedia.org/wiki/Multi-factor_authentication) app
1. Enter the code your 2FA app displays in the text field *code*
1. Click *Confirm two-factor authentication*

Once you logged out, the Customer Control Panel can only be accessed via a 2FA code your app generates.

#### DNS

Via Domains -> *magnifying glass* -> DNS:

1. Set *TTL* to a low value (but at least `300` seconds since some DNS servers do not process lower values) to quickly recover from a wrong DNS configuration
1. Enable *DNSSEC Status* to increase the security
1. Add DNS records as follows (the `TLSA` records can only be set after a TLS certificate is created via Ansible by replacing the fingerprints with the one returned in the end of the Ansible [Remote playbook](#remote-playbook)):
    Host | Type | MX | Destination | Description
    ---|---|---|---|---
    `*` | `A` | | `45.157.179.214` | Point to the specified IPv4 address for all third-level domains (e.g., `chat.xmpp-providers.org`)
    `*` | `AAAA` | | `2a03:4000:4b:b7::1` | Point to the specified IPv6 address for all third-level domains (e.g., `chat.xmpp-providers.org`)
    `@` | `A` | | `45.157.179.214` | Point to the specified IPv4 address for `xmpp-providers.org`
    `@` | `AAAA` | | `2a03:4000:4b:b7::1` | Point to the specified IPv6 address for `xmpp-providers.org`
    `@` | `CAA` | | `128 issue "letsencrypt.org"` | Advise a Certificate Authority (CA) to issue a TLS certificate for `xmpp-providers.org` and its subdomains only if the CA is specified there (see Certificate Authority Authorization ([CAA](https://letsencrypt.org/docs/caa/)))
    `_443._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a client connecting to `xmpp-providers.org:443` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_3478._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a server connecting to `xmpp-providers.org:3478` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_5222._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a client connecting to `xmpp-providers.org:5222` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_5223._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a client connecting to `xmpp-providers.org:5223` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_5269._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a server connecting to `xmpp-providers.org:5269` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_5270._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a server connecting to `xmpp-providers.org:5270` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_5349._tcp` | `TLSA` | | `3 1 1 4629962ac7f951db88f7bc52d12bb6c366eda3558c677566c3aa2b373377e212` | Advise a server connecting to `xmpp-providers.org:3478` to exclusively accept the specified public key of the used TLS certificate, see [TLSA](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities#TLSA_RR)
    `_xmpp-client._tcp` | `SRV` | | `1 0 5222 xmpp-providers.org` | Point to `xmpp-providers.org:5222` for STARTTLS-secured XMPP client-to-server (C2S) connections and specify the destination as second-most preferred (the lower the first number the higher the priority, see [XEP-0368](https://xmpp.org/extensions/xep-0368.html))
    `_xmpp-client._tcp` | `SRV` | | `3 0 443 xmpp-providers.org` | Point to `xmpp-providers.org:443` for STARTTLS-secured XMPP client-to-server (C2S) connections and specify the destination as fourth-most preferred (the lower the first number the higher the priority, see [XEP-0368](https://xmpp.org/extensions/xep-0368.html))
    `_xmpp-server._tcp` | `SRV` | | `1 0 5269 xmpp-providers.org` | Point to `xmpp-providers.org:5269` for STARTTLS-secured XMPP server-to-server (S2S) connections and specify the destination as second-most preferred (the lower the first number the higher the priority, see [XEP-0368](https://xmpp.org/extensions/xep-0368.html))
    `_xmpps-client._tcp` | `SRV` | | `0 0 5223 xmpp-providers.org` | Point to `xmpp-providers.org:5222` for Direct-TLS-secured XMPP client-to-server (C2S) connections and specify the destination as most preferred (the lower the first number the higher the priority, see [XEP-0368](https://xmpp.org/extensions/xep-0368.html))
    `_xmpps-client._tcp` | `SRV` | | `2 0 443 xmpp-providers.org` | Point to `xmpp-providers.org:443` for Direct-TLS-secured XMPP client-to-server (C2S) connections and specify the destination as third-most preferred (the lower the first number the higher the priority, see [XEP-0368](https://xmpp.org/extensions/xep-0368.html))
    `_xmpps-server._tcp` | `SRV` | | `0 0 5270 xmpp-providers.org` | Point to `xmpp-providers.org:5270` for Direct-TLS-secured XMPP server-to-server (S2S) connections and specify the destination as most preferred (the lower the first number the higher the priority, see [XEP-0368](https://xmpp.org/extensions/xep-0368.html))

### Server Control Panel

This section describes the steps on the Server Control Panel ([SCP](https://www.servercontrolpanel.de)).
Thy are done to install an operating system on the server and to configure the network settings.

#### Secure Access

Via *top menu* -> Options -> Settings:

1. Enable *secure access mode*
1. Enter the Server Control Panel password in *Password to confirm*

Once you logged out, the Server Control Panel can only be accessed via the *SCP Auto-Login* button after logging in to the [Customer Control Panel](#customer-control-panel).
That way, the Customer Control Panel's 2FA protects the access to the Server Control Panel as well.

#### rDNS Records

Via *server selection* -> Network:

1. Add an rDNS record for IPv4 with the following content:
    * IP: `45.157.179.214/22`
    * Gateway / Routing to: `45.157.176.1`
    * rDNS: `xmpp-providers.org`
1. Add an rDNS record for IPv6 with the following content:
    * IP: `2a03:4000:4b:b7::1`
    * Gateway / Routing to: `xmpp-providers.org`

#### Installation

Via *server selection* -> Media -> Images -> Official Images:

1. Select Debian stable (e.g., *Debian (12) Bookworm*)
1. Click *Minimal / minimal system with ssh preinstalled* and choose the following options:
    * Partition layout: *one big partition with os as root partition*
    * Hostname: `xmpp-providers.org`
    * Locale *en_US.UTF-8*
    * Timezone: *Europe/Berlin*
    * Define a User: *Yes*
    * username: `<username>` (replace `<username>` with the user to be created on the server, i.e., the server user)
    * User Password: `<password>` (replace `<password>` with the password of the user to be created)
    * SSH Key: *No*
    * Activate SSH password: *Yes*
1. Save the entered username and password to use them later
1. Click *Next*
1. Click *reinstall*
1. Save the server's displayed **ED25519** public SSH key **SHA256** fingerprint to enter it later when connecting to the server the first time (Example: Save `SHA256:StPKQlWgh4eHuF9jISXSq03xS3bG+1dO46JHazYWSos` of the line `256 SHA256:StPKQlWgh4eHuF9jISXSq03xS3bG+1dO46JHazYWSos (ED25519)`)
1. Save the new root password in case you need to use it later
