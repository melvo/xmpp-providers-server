#!/bin/sh

# SPDX-FileCopyrightText: 2024 Melvin Keskin <melvo@olomono.de>
#
# SPDX-License-Identifier: CC0-1.0

# This script runs the Ansible playbook for configuring the local setup.

file_directory_path=$(dirname "$(readlink -f "${0}")")
ansible_directory_path="${file_directory_path}/ansible"
ansible_playbook_filename="local_playbook.yml"

# Clear Ansible's cache to circumvent occasional hanging on the execution of a task.
rm -fr ~/.ansible/cp ~/.ansible/tmp

# Change to the Ansible directory to use its configuration file.
cd "${ansible_directory_path}"

# Run the playbook.
ansible-playbook "${ansible_playbook_filename}" "${@}"
