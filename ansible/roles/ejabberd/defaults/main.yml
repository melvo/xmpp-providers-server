---

# Path of the URL for administration via a web interface.
# Example: "/admin" would result in "https://example.org/admin".
# Set "" to disable the web administration.
ejabberd_web_administration_url_path: "/admin"

# Path of the URL for an XMPP web client.
# Example: "/chat" would result in "https://example.org/chat".
# Set "" to disable the web client.
ejabberd_web_client_url_path: "/chat"

# Path of the URL for an HTTP API.
# Example: "/api" would result in "https://example.org/api".
# Set "" to disable the HTTP API.
ejabberd_http_api_url_path: "/api"

# Account creation possibilities for new users.
# Possible values:
#   "none": Do not allow account creation.
#   "in_band": Allow creating accounts via XMPP clients.
#   "web": Allow creating accounts via a web page on "/account/new" (e.g., https://example.org/account/new).
#   "all": "in_band" and "web" together.
ejabberd_account_creation: "none"

# Whether a CAPTCHA must be solved to create an account via an XMPP client.
# ejabberd does not allow to change the CAPTCHA usage solely for creating accounts via a web page.
ejabberd_protect_in_band_account_creation_by_captcha: false

# Hash algorithm used by SCRAM.
# Possible values: See https://docs.ejabberd.im/admin/configuration/toplevel/#auth_scram_hash
ejabberd_scram_hash_algorithm: "sha512"

# Maximum size of all shared files in total per user (in bytes).
ejabberd_maximum_http_file_upload_file_size: 100000000

# JIDs that are allowed to do privileged actions.
ejabberd_admin_jids: []

# Address URIs for users to contact the admins.
ejabberd_admin_addresses: []

# Address URIs for users to report abuse.
ejabberd_abuse_addresses: []

# Address URIs for users to get support.
ejabberd_support_addresses: []

# Password of the database in that ejabberd stores its data.
ejabberd_db_password: ""

# Users of this group get access to the generated certificates.
ejabberd_tls_group: "tls"

# IPv4 address of the server.
ejabberd_ipv4_address: ""

# IPv6 address of the server.
ejabberd_ipv6_address: ""

# Subdomains used for various XMPP services.
ejabberd_subdomains:
  # XEP-0369
  mix: "mix"
  # XEP-0045
  muc: "muc"
  # XEP-0065
  proxy: "proxy"
  # XEP-0060
  pubsub: "pubsub"
  # XEP-0363
  upload: "upload"

# Whether messages from unknown senders are blocked.
# If enabled, messages are blocked until the sender requests the recipient's presence, solves a CAPTCHA and is allowed by the recipient to receive their presence.
# Senders on the same server are never blocked.
ejabberd_block_strangers: false

# Whether the server's operating system and the XMPP server's software (including its version) can be queried.
ejabberd_reveal_software_details: true

# Repository containing backported software.
ejabberd_backports_repository: "{{ apt_backports_repository }}"
